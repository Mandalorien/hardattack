<!DOCTYPE html PUBLIC "-//W3C//DTD XHTML 1.0 Strict//EN" "http://www.w3.org/TR/xhtml1/DTD/xhtml1-strict.dtd">
<html xmlns="http://www.w3.org/1999/xhtml" xml:lang="fr">
<!-- Cette page est valide W3C -->
	<!-- Debut du head -->
	<head>
		<!-- Title de la page -->
		<title>{title}</title>
			<link rel="shortcut icon" href="favicon.ico"/>
			<meta name="viewport" content="width=device-width, initial-scale=1">
			<meta content="text/html; charset=utf-8" http-equiv="Content-Type" />
			<meta content="fr" http-equiv="Content-language"/>
			<meta content="mandalorien" name="author"/>
			<meta content="mandalorien" name="copyright"/>
			<meta content="jeux par naviguateur web" name="description"/>
			<meta content="jeux,naviguateur" name="keywords"/>
			<meta content="never" name="Expires"/>
			<meta content="Tous public" name="rating"/>
			<meta content="HardAttack" name="subject"/>
			{style}
	</head>
	<body>