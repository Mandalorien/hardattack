<?php
/**
 * This file is part of HardAttack
 *
 * @license none
 *
 * Copyright (c) 2014-Present, mandalorien
 * All rights reserved.
 *=========================================================
  _    _               _         _   _             _    
 | |  | |             | |   /\  | | | |           | |   
 | |__| | __ _ _ __ __| |  /  \ | |_| |_ __ _  ___| | __
 |  __  |/ _` | '__/ _` | / /\ \| __| __/ _` |/ __| |/ /
 | |  | | (_| | | | (_| |/ ____ \ |_| || (_| | (__|   < 
 |_|  |_|\__,_|_|  \__,_/_/    \_\__|\__\__,_|\___|_|\_\                                                                                                        
 *=========================================================
 *
 * create 2013 by mandalorien
 */
	class DatabaseConnection
	{
			Private $_host;
			private $_user;
			private $_password;
			private $_db;
			private $_actif;
			
			Public function __construct($host,$user,$password,$database,$actif)
			{
				$this->_host = $host;
				$this->_user = $user;
				$this->_password = $password;
				$this->_db = $database;
				$this->_actif = $actif;
			}
			
			Public function Connexion()
			{
				if($this->_actif == true)
				{
					try
					{
						$db = new PDO("mysql:host={$this->_host};dbname={$this->_db}","{$this->_user}","{$this->_password}");
						$db->setAttribute(PDO::ATTR_ERRMODE, PDO::ERRMODE_EXCEPTION);
						$db->exec('SET NAMES UTF8');
					}
					catch (PDOException $e)
					{
						trigger_error("<code>Error code : {$e->getCode()}" . PHP_EOL . "<br />Error message : {$e->getMessage()}" . PHP_EOL . "</code><br />" . PHP_EOL, E_USER_WARNING);
					}
					return $db;
				}
				else
				{
					return false;
				}
			}
	}
?>