<?php

/**
 * This file is part of HardAttack
 *
 * @license none
 *
 * Copyright (c) 2014-Present, mandalorien
 * All rights reserved.
 *=========================================================
  _    _               _         _   _             _    
 | |  | |             | |   /\  | | | |           | |   
 | |__| | __ _ _ __ __| |  /  \ | |_| |_ __ _  ___| | __
 |  __  |/ _` | '__/ _` | / /\ \| __| __/ _` |/ __| |/ /
 | |  | | (_| | | | (_| |/ ____ \ |_| || (_| | (__|   < 
 |_|  |_|\__,_|_|  \__,_/_/    \_\__|\__\__,_|\___|_|\_\                                                                                                        
 *=========================================================
 *
 * create 2013 by mandalorien
 */
 
$Base_features =  array
(
	    Types::HUNTER => array(
		Features::NAME      => "Chasseur",
        Features::HULL      => 20,
        Features::SHIELD    => 45,
        Features::WEAPONS  => 13,
        Features::HEALTH     => 50,
        Features::SIZE => 10,
		Features::SPEEDFIGHT => 15,
		Features::PRICES => 400,
		Features::INIATIVE => 10
        ),
		
	    Types::CRUISER => array(
		Features::NAME      => "Croiseur",
        Features::HULL      => 45,
        Features::SHIELD    => 70,
        Features::WEAPONS  => 18,
        Features::HEALTH     => 100,
        Features::SIZE => 35,
		Features::SPEEDFIGHT => 5,
		Features::PRICES => 750,
		Features::INIATIVE => 8
        ),
		
	    Types::TRACKER => array(
		Features::NAME      => "Traqueur",
        Features::HULL      => 35,
        Features::SHIELD    => 60,
        Features::WEAPONS  =>  32,
        Features::HEALTH     => 75,
        Features::SIZE => 20,
		Features::SPEEDFIGHT => 7,
		Features::PRICES => 1000,
		Features::INIATIVE => 11
        ),
		
	    Types::DESTROYER => array(
		Features::NAME      => "Destroyer",
        Features::HULL      => 60,
        Features::SHIELD    => 80,
        Features::WEAPONS  => 27,
        Features::HEALTH     => 85,
        Features::SIZE => 40,
		Features::SPEEDFIGHT => 4,
		Features::PRICES => 1500,
		Features::INIATIVE => 3
        ),
		
	    Types::PREDATORS => array(
		Features::NAME      => "Predateur",
        Features::HULL      => 40,
        Features::SHIELD    => 50,
        Features::WEAPONS  => 30,
        Features::HEALTH     => 60,
        Features::SIZE => 30,
		Features::SPEEDFIGHT => 9,
		Features::PRICES => 2000,
		Features::INIATIVE => 7
        )
);