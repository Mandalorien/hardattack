<?php

class Ships
{
	private $_idship;
	private $_name;//nom
	private $_hull;//coque
	private $_shield;//bouclier
	private $_weapon; //armes
	private $_power;//puissance
	private $_health;//vie
	private $_type;//le type du vaisseau chasseur/croiseur etc...
	private $_size;//taille du vaisseaux
	private $_speedfight;//vitesse de tire
	private $_prices;//vitesse de tire
	private $_iniative;//vitesse de tire
	private $_idplayer;//id du joueur
	
	// aller on inialise :D
	public function __construct($idship,$type,$idplayer)
	{	
		global $Base_features;
		
		$this->_idship = $idship;
		$this->_name = $Base_features[$type]["name"];
		$this->_hull = $Base_features[$type]["hull"];
		$this->_shield = $Base_features[$type]["shield"];
		$this->_weapon = $Base_features[$type]["weapon"];
		$this->_power = 0;
		$this->_health = $Base_features[$type]["health"];
		$this->_type = $type;
		$this->_size = $Base_features[$type]["size"];
		$this->_speedfight = $Base_features[$type]["speedfight"];
		$this->_prices = $Base_features[$type]["prices"];
		$this->_iniative = $Base_features[$type]["iniative"];
		$this->_idplayer = $idplayer;
	}
	
	/* Getter ou mutateur */
	Public function Get_Idship()
	{
		return $this->_idship;
	}
	
	Public function Get_Name()
	{
		return $this->_name;
	}
	
	Public function Get_Hull()
	{
		return $this->_hull;
	}
	
	Public function Get_Shield()
	{
		return $this->_shield;
	}
	
	Public function Get_Weapon()
	{
		return $this->_weapon;
	}
	
	Public function Get_Power()
	{
		return $this->_power;
	}
	
	Public function Get_Health()
	{
		return $this->_health;
	}
	
	Public function Get_Type()
	{
		return $this->_type;
	}
	
	Public function Get_Size()
	{
		return $this->_size;
	}
	
	Public function Get_SpeedFight()
	{
		return $this->_speedfight;
	}
	
	
	Public function Get_Prices()
	{
		return $this->_prices;
	}
	
	Public function Get_Iniative()
	{
		return $this->_iniative;
	}
	
	Public function Get_IdPlayer()
	{
		return $this->_idplayer;
	}
	
	/* setter ou acesseur */
	Public function Set_Idship($valeur)
	{
		$this->_idship = $valeur;
	}
	
	Public function Set_Name($libelle)
	{
		$this->_name = $libelle;
	}
	
	Public function Set_Hull($valeur)
	{
		$this->_hull = $valeur;
	}
	
	Public function Set_Shield($valeur)
	{
		$this->_shield = $valeur;
	}
	
	Public function Set_Weapon($valeur)
	{
		$this->_weapon = $valeur;
	}
	
	Public function Set_Power($valeur)
	{
		$this->_power = $valeur;
	}
	
	Public function Set_Health($valeur)
	{
		$this->_health = $valeur;
	}
	
	Public function Set_Type($valeur)
	{
		$this->_type = $valeur;
	}
	
	Public function Set_Size($valeur)
	{
		$this->_size = $valeur;
	}
	
	Public function Set_SpeedFight($floater)
	{
		$this->_speedfight = $floater;
	}
	
	Public function Set_Prices($valeur)
	{
		$this->_prices = $valeur;
	}
	
	Public function Set_Iniative($valeur)
	{
		$this->_iniative = $valeur;
	}
	
	Public function Set_IdPlayer($valeur)
	{
		$this->_idplayer = $valeur;
	}
	
	/* méthodes */
	public function amelioration($name,$hull,$shield,$weapon,$health,$type,$size,$speedfight,$iniative,$prices)
	{
		$this->_name = $name .' - '.$Base_features[$type]["name"];
		$this->_hull = $hull + $Base_features[$type]["hull"];
		$this->_shield = $shield + $Base_features[$type]["shield"];
		$this->_weapon = $weapon + $Base_features[$type]["weapon"];
		$this->_health = $health + $Base_features[$type]["health"];
		$this->_size = $size + $Base_features[$type]["size"];
		$this->_speedfight = $speedfight + $Base_features[$type]["speedfight"];
		$this->_iniative = $iniative + $Base_features[$type]["iniative"];
		$this->_prices = $prices + $Base_features[$type]["prices"];
	}
	
	/* méthodes */
	public function puissance($weapon,$speedfight)
	{
		$puissance = intval($weapon) * floatval($speedfight);
		return $puissance;
	}
	
	/* méthodes */
	public function prices_update()
	{
		$Prices_update = $Prices_update + floatval($this->_hull/10) + floatval($this->_shield/5) + floatval($this->_weapon/10) + floatval($this->_health/25);
		return $Prices_update;
	}
}
?>