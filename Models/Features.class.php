<?php

/**
 * This file is part of HardAttack
 *
 * @license none
 *
 * Copyright (c) 2014-Present, mandalorien
 * All rights reserved.
 *=========================================================
  _    _               _         _   _             _    
 | |  | |             | |   /\  | | | |           | |   
 | |__| | __ _ _ __ __| |  /  \ | |_| |_ __ _  ___| | __
 |  __  |/ _` | '__/ _` | / /\ \| __| __/ _` |/ __| |/ /
 | |  | | (_| | | | (_| |/ ____ \ |_| || (_| | (__|   < 
 |_|  |_|\__,_|_|  \__,_/_/    \_\__|\__\__,_|\___|_|\_\                                                                                                        
 *=========================================================
 *
 * create 2013 by mandalorien
 */
 
class Features
{
	const NAME = "name";
	const HULL = "hull";// coque
	const SHIELD = "shield"; // bouclier
	const WEAPONS = "weapon"; // Armes
	const HEALTH = "health"; // vie
	const SIZE = "size"; // taille
	const SPEEDFIGHT = "speedfight"; // vitesse d'attaques
	const PRICES = "prices"; // prix
	const INIATIVE = "iniative"; // iniative
}
?>