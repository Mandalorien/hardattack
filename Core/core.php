<?php
/**
 * This file is part of HardAttack
 *
 * @license none
 *
 * Copyright (c) 2014-Present, mandalorien
 * All rights reserved.
 *=========================================================
  _    _               _         _   _             _    
 | |  | |             | |   /\  | | | |           | |   
 | |__| | __ _ _ __ __| |  /  \ | |_| |_ __ _  ___| | __
 |  __  |/ _` | '__/ _` | / /\ \| __| __/ _` |/ __| |/ /
 | |  | | (_| | | | (_| |/ ____ \ |_| || (_| | (__|   < 
 |_|  |_|\__,_|_|  \__,_/_/    \_\__|\__\__,_|\___|_|\_\                                                                                                        
 *=========================================================
 *
 * create 2013 by mandalorien
 */
 
session_start();

ini_set('display_errors',true);

define('ROOT_PATH', dirname(dirname(__FILE__)) . DIRECTORY_SEPARATOR);//racine dsu dossier
define('CONTROLLER',ROOT_PATH .'Controllers/');
define('CORE',ROOT_PATH .'Core/');
define('INCLUDES',CORE .'includes/');
define('CONNECT',ROOT_PATH .''. CORE .'connect/');
define('LANGUAGES',ROOT_PATH .'Languages/');
define('MODELS',ROOT_PATH .'Models/');
define('VUES',ROOT_PATH .'Vues/');

define('NAME_SITE','HardAttack');
define('SITEURL','http://localhost/HardAttack/');
define('CSS',SITEURL .'Styles/');
define('IMAGES',SITEURL .'Images/');
define('JQUERY',SITEURL .'Jquerys/');
define('SCRIPT',SITEURL .'Scripts/');

define('TEMPLATE_DIR', realpath(VUES . 'Template/'));
define('TEMPLATE_NAME', 'default');


define('DEFAULT_LANG', 'fr');

require_once(INCLUDES .'function/ListPages.php'); # la listes des pages

require_once(INCLUDES .'function/date.php'); # function de date fr
require_once(INCLUDES .'function/Template.php'); # function de Template primordiale
require_once(INCLUDES .'function/Langue.php'); # function de Langue primordiale
require_once(INCLUDES .'functions.php'); # les differentes functions majeurs 
require_once(INCLUDES .'function/Pages.php'); # class Pages
require_once(MODELS .'Types.class.php'); # les types de vaisseaux
require_once(MODELS .'Features.class.php');# les caractéristiques des vaisseaux
require_once(MODELS .'Base_Features.php');# les paramétres par défaults
require_once(MODELS .'Ships.class.php');# la classes vaisseaux

//on inclus le fichier config
/*require_once(CONNECT .'configs.php');*/

require_once(INCLUDES .'Loader.php'); # chargement des vaisseaux
?>