<?php
/**
 * This file is part of HardAttack
 *
 * @license none
 *
 * Copyright (c) 2014-Present, mandalorien
 * All rights reserved.
 *=========================================================
  _    _               _         _   _             _    
 | |  | |             | |   /\  | | | |           | |   
 | |__| | __ _ _ __ __| |  /  \ | |_| |_ __ _  ___| | __
 |  __  |/ _` | '__/ _` | / /\ \| __| __/ _` |/ __| |/ /
 | |  | | (_| | | | (_| |/ ____ \ |_| || (_| | (__|   < 
 |_|  |_|\__,_|_|  \__,_/_/    \_\__|\__\__,_|\___|_|\_\                                                                                                        
 *=========================================================
 *
 * create 2013 by mandalorien
 */
/* 3 paramétres :
 * id du vaisseau de type integer
 * le type
 * id du players
 */
$ships = new Ships(1,1,1);
$Weapon = $ships->Get_Weapon();
$Speedfight = $ships->Get_SpeedFight();
$puissance = $ships->puissance($Weapon,$Speedfight);
$ships->Set_Power($puissance);
// $ships->prices_update();
// var_dump($ships);
// var_dump($puissance);
// var_dump($puissance);