<?php
/**
 * This file is part of HardAttack
 *
 * @license none
 *
 * Copyright (c) 2014-Present, mandalorien
 * All rights reserved.
 *=========================================================
  _    _               _         _   _             _    
 | |  | |             | |   /\  | | | |           | |   
 | |__| | __ _ _ __ __| |  /  \ | |_| |_ __ _  ___| | __
 |  __  |/ _` | '__/ _` | / /\ \| __| __/ _` |/ __| |/ /
 | |  | | (_| | | | (_| |/ ____ \ |_| || (_| | (__|   < 
 |_|  |_|\__,_|_|  \__,_/_/    \_\__|\__\__,_|\___|_|\_\                                                                                                        
 *=========================================================
 *
 * create 2013 by mandalorien
 */

// Fonction de lecture / ecriture / exploitation de templates

function ReadFromFile($filename) {
    $content = @file_get_contents($filename);
    return $content;
}

function saveToFile($filename, $content) {
    $content = file_put_contents($filename, $content);
}

function parsetemplate($template, $array) {
	if(floatval(phpversion()) < 4.0)
	{
		return preg_replace('#\{([a-z0-9\-_]*?)\}#Ssie', '( ( isset($array[\'\1\']) ) ? $array[\'\1\'] : \'\' );', $template);
	}
	else
	{
		$page = preg_replace_callback(
                    "#\{([a-z0-9\-_]*?)\}#Ssi",
                    function ($m) use ($array) { return $array[$m[1]]; } , 
                    $template);
		return $page;
	}
}

function getTemplate($templateName) {

    $filename = TEMPLATE_DIR . '/' . TEMPLATE_NAME . "/{$templateName}.tpl";
    return ReadFromFile($filename);
}
?>