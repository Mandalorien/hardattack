<?php
/**
 * This file is part of HardAttack
 *
 * @license none
 *
 * Copyright (c) 2014-Present, mandalorien
 * All rights reserved.
 *=========================================================
  _    _               _         _   _             _    
 | |  | |             | |   /\  | | | |           | |   
 | |__| | __ _ _ __ __| |  /  \ | |_| |_ __ _  ___| | __
 |  __  |/ _` | '__/ _` | / /\ \| __| __/ _` |/ __| |/ /
 | |  | | (_| | | | (_| |/ ____ \ |_| || (_| | (__|   < 
 |_|  |_|\__,_|_|  \__,_/_/    \_\__|\__\__,_|\___|_|\_\                                                                                                        
 *=========================================================
 *
 * create 2013 by mandalorien
 */

/**** on met opera appart ****/
function Date_fr(){
	date_default_timezone_set('Europe/Paris'); #joli fuseau horaire
	$temps = time();

	// JOURS
	$jours = array('Dimanche', 'Lundi', 'Mardi', 'Mercredi', 'Jeudi', 'Vendredi', 'Samedi');
	$jours_numero = date('w', $temps);
	$jours_complet = $jours[$jours_numero];
	// Numero du jour
	$NumeroDuJour = date('d', $temps);

	// MOIS
	$mois = array('', 'Janvier', 'Février', 'Mars', 'Avril', 'Mai',
	'Juin', 'Juillet', 'Août', 'Septembre', 'Octobre', 'Novembre', 'Décembre');
	$mois_numero = date("m", $temps);
	$numm = intval($mois_numero);
	$mois_complet = $mois[$numm];

	// ANNEE
	$annee = date("Y", $temps);
	return $jours_complet.' '.$NumeroDuJour.' '.$mois_complet.' '.$annee.' il est '.date('H:i:s'); // Retourne True si ses un mobile sinon False.
}
?>