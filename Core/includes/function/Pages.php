<?php
/**
 * This file is part of HardAttack
 *
 * @license none
 *
 * Copyright (c) 2014-Present, mandalorien
 * All rights reserved.
 *=========================================================
  _    _               _         _   _             _    
 | |  | |             | |   /\  | | | |           | |   
 | |__| | __ _ _ __ __| |  /  \ | |_| |_ __ _  ___| | __
 |  __  |/ _` | '__/ _` | / /\ \| __| __/ _` |/ __| |/ /
 | |  | | (_| | | | (_| |/ ____ \ |_| || (_| | (__|   < 
 |_|  |_|\__,_|_|  \__,_/_/    \_\__|\__\__,_|\___|_|\_\                                                                                                        
 *=========================================================
 *
 * create 2013 by mandalorien
 */
 
class Pages
{
	public function __construct($Page) # Constructeur demandant 1 paramètres
	{
		global $lang;
		includeLang('wipeout'); # on inclus la langue pour pas le répété
		$this->LoadPage($Page,$lang); # Initialisation de la page.
	}
	
	public function LoadPage($Page,$lang) # Une méthode qui chargera la page demander $page serra un tableau
	{
		# simple sécurité
		if(isset($_REQUEST['page']))
		{
			$mapage = $_REQUEST['page'];
		}
		elseif(!isset($mapage))
		{
			$mapage = "Accueil";
		}
		
		# les pages qu'on va autorisé , j'aurai plus faire un switch mais bon ...
		if(in_array($mapage,$Page))
		{
			include_once dirname(dirname(dirname(dirname(__FILE__)))) .'/Controllers/'.$mapage.'.php';
			return;
		}
		else
		{
			include_once dirname(dirname(dirname(dirname(__FILE__)))) .'/Controllers/init.php';
			return;
		}
	}
}
?>