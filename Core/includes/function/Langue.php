<?php
/**
 * This file is part of HardAttack
 *
 * @license none
 *
 * Copyright (c) 2014-Present, mandalorien
 * All rights reserved.
 *=========================================================
  _    _               _         _   _             _    
 | |  | |             | |   /\  | | | |           | |   
 | |__| | __ _ _ __ __| |  /  \ | |_| |_ __ _  ___| | __
 |  __  |/ _` | '__/ _` | / /\ \| __| __/ _` |/ __| |/ /
 | |  | | (_| | | | (_| |/ ____ \ |_| || (_| | (__|   < 
 |_|  |_|\__,_|_|  \__,_/_/    \_\__|\__\__,_|\___|_|\_\                                                                                                        
 *=========================================================
 *
 * create 2013 by mandalorien
 */


/**
 * Gestion de la localisation des chaînes
 *
 * @param string $filename
 * @param string $extension
 * @return void
 */
function includeLang($filename, $extension = '.php')
{
    global $lang, $user;

    $pathPattern = LANGUAGES ."%s/{$filename}%s";
    if (isset($user['lang']) && !empty($user['lang'])) {
        if($fp = @fopen($filename = sprintf($pathPattern, $user['lang'], '.csv'), 'r', true)) {
            fclose($fp);

            require_once $filename;
            return;
        } else if ($fp = @fopen($filename = sprintf($pathPattern, $user['lang'], $extension), 'r', true)) {
            fclose($fp);

            require_once $filename;
            return;
        }
    }

    require_once sprintf($pathPattern, DEFAULT_LANG,$extension);
    return;
}
?>