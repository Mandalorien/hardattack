<?php
/**
 * This file is part of HardAttack
 *
 * @license none
 *
 * Copyright (c) 2014-Present, mandalorien
 * All rights reserved.
 *=========================================================
  _    _               _         _   _             _    
 | |  | |             | |   /\  | | | |           | |   
 | |__| | __ _ _ __ __| |  /  \ | |_| |_ __ _  ___| | __
 |  __  |/ _` | '__/ _` | / /\ \| __| __/ _` |/ __| |/ /
 | |  | | (_| | | | (_| |/ ____ \ |_| || (_| | (__|   < 
 |_|  |_|\__,_|_|  \__,_/_/    \_\__|\__\__,_|\___|_|\_\                                                                                                        
 *=========================================================
 *
 * create 2013 by mandalorien
 */

// ----------------------------------------------------------------------------------------------------------------
//
// Routine d'affichage d'une page dans un cadre donnée
//
// $page      -> la page
// $title     -> le titre de la page

function display ($page, $title = NAME_SITE)
{
	global $link,$db;
	
	$DisplayPage  = StdUserHeader ($title)." \n";
	$DisplayPage  .= ShowCorp($page)." \n";

	$DisplayPage .= StdFooter();
	echo $DisplayPage;
	die();
}

// ----------------------------------------------------------------------------------------------------------------
//
// Entete de page
//
function StdUserHeader ($title) 
{
	global  $lang;
	$parse['Link'] = SITEURL;
	$parse['style']  = "<link rel=\"stylesheet\" type=\"text/css\" href=\"".CSS."style.css\">\n";
	$parse['style']  .= "<link rel=\"stylesheet\" type=\"text/css\" href=\"".CSS."bootstrap.min.css\">\n";
	$parse['style']  .= "<script type=\"text/javascript\" src=\"".JQUERY."jquery-1.10.2.min.js\"></script>\n";
	$parse['style']  .= "<script type=\"text/javascript\" src=\"".JQUERY."bootstrap.min.js\"></script>\n";
	$parse['title'] = $title;
	return parsetemplate(getTemplate('simple_header'), $parse);

}

// ----------------------------------------------------------------------------------------------------------------
//
// Menu de page

function ShowMenu()
{
	global $lang,$ListPages;
	
	$vlp = "";
	$compteur = 1;
	$first = each($ListPages);
	foreach($ListPages as $vuepages)
	{
		if($compteur == 1)
		{
			$vlp .="<li class='first'><a href='index.php?page=".$vuepages."' title='".$lang[$vuepages]."'>".$vuepages."</a></li>";
		}
		else
		{
			$vlp .="<li><a href='index.php?page=".$vuepages."' title='".$lang[$vuepages]."'>".$vuepages."</a></li>";
		}
		$compteur ++;
	}
	$parse = $lang;
	$parse['listpage'] = $vlp;
	$parse['Name_Game'] = NAME_SITE;
	return parsetemplate(gettemplate('menu'), $parse);	      
}

// ----------------------------------------------------------------------------------------------------------------
//
// Corp de page

function ShowCorp($page)
{
	global  $lang;
	$DisplayCorp = ShowMenu();
	$DisplayCorp .= $page;
	return $DisplayCorp;
}

// ----------------------------------------------------------------------------------------------------------------
//
// Pied de page

function StdFooter() {
	global  $lang;
	
	$parse['link'] =SITEURL;
	$parse['copyright'] ='&copy; 2014 '. NAME_SITE .' ';
	$parse['credit'] ='Credits';
	$parse['design'] ='design by Mandalorien ';
	$parse['time'] = Date_fr();
	return parsetemplate(gettemplate('overall_footer'), $parse);
}
?>